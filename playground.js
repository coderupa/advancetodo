let todos=[
  {id:1,item:'Todo Item #1',done:true,category:'General'},
  {id:2,item:'Todo Item #2',done:false,category:'General'},
  {id:3,item:'Todo Item #3',done:false,category:'Work'},
  {id:4,item:'Todo Item #4',done:false,category:'Project'},
]

let item= {id:5,item:'Todo Item #5',done:false,category:'Home'}


let update=[item,...todos]
//let updatex = item //[...todos,item]
// console.log(update)


var myArray = [{
  name: "Joe Blow",
  date: "Mon Oct 31 2016 00:00:00 GMT-0700 (PDT)"
}, {
  name: "Sam Snead",
  date: "Sun Oct 30 2016 00:00:00 GMT-0700 (PDT)"
}, {
  name: "John Smith",
  date: "Sat Oct 29 2016 00:00:00 GMT-0700 (PDT)"
}];

myArray.sort(function compare(a, b) {
  var dateA = new Date(a.date);
  var dateB = new Date(b.date);
  return dateA - dateB;
});

console.log(myArray);

