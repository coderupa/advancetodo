import React, { Component } from 'react'
import { View,StyleSheet } from 'react-native'
import {
  List,
  ListItem,
  CheckBox,
  Body,
  Text,
  Icon,
  Button,
  Left,
  Right
} from 'native-base'

const itemStyle=(done)=>{
  if (done==true){
    return {
      textDecorationLine: 'line-through'
    }
  }
}

const TodoItem =({id,item,done,actionFinish,actionDelete,category,date})=>(
  <ListItem icon noBorder style={{marginLeft:0,marginBottom:15,padding:10}} >
    <Left>
      <CheckBox checked={Boolean(done)} onPress={()=>actionFinish(id)}/>
    </Left>
    <Body style={{borderBottomWidth:null,marginLeft:10}}>
      <Text style={itemStyle(done)}>{item}</Text>
      <Text note> {category}</Text>
    </Body>
    <Right style={{borderBottomWidth:null}}>
      <Icon  onPress={()=>actionDelete(id)} name="md-trash"/>
    </Right>
  </ListItem>
)

/*
<Button title='Finish' onPress={()=>actionFinish(id)}/>
<Button title='Delete' onPress={()=>actionDelete(id)}/>
*/

export default class TodoItems extends Component {
  state = {  }
  render() {
    let todos=this.props.todos
    let {actionDelete,actionFinish} = this.props
    return (
      <View style={styles.Container}>
      {
        todos.map((todo)=>(
          <TodoItem 
            key={todo.id} 
            actionDelete={actionDelete} 
            actionFinish={actionFinish} 
            {...todo} />
        ))
      }
 

      </View>
    )
  }
}

const styles = StyleSheet.create({
  Container:{
    // flex:1
  },
  TodoItem:{
    // flexDirection:'row',
    // alignItems:'center',
    // borderBottomWidth:1,
    // borderColor:'#F1F1F1',
    // padding:15,
  }
});