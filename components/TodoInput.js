import React, { Component } from 'react'
import { View,StyleSheet,Platform } from 'react-native'
import {
  Input,
  Item,
  Icon,
  Button,
  Text,
  Radio,
  Picker,
  Label
} from 'native-base'

export default class TodoInput extends Component {
  state={
    item:'',
    showCategory:false,
    selectedCategory:'General'
  }
  _addItem(){
      this.props.actionAdd(this.state.item,this.state.selectedCategory)
      this.setState({item:''})
  }
  _actions(){
    return(
      <View style={{flexDirection:'row'}}>
        <Icon color='transparent' name='md-add' onPress={()=> this._addItem()} style={{marginRight:10}}/>
        <Icon color='transparent' name='md-create' onPress={()=> this.setState({showCategory:!this.state.showCategory})} />
      </View>
    )
  }
  _categoriesForm(){
    return(
      <Item style={{marginBottom:30}}>
        <Icon name="md-albums" style={{fontSize:30,color:"#CCC"}}/>
        <Label> Category : </Label>
        <Picker
          style={{ width:(Platform.OS === 'ios') ? undefined : 220 }}
          iosHeader="Select Category"
          headerBackButtonText= {<Icon name="ios-arrow-back" style={{color:"#FFF",fontSize:20,fontWeight:'bold'}}/>}
          mode="dropdown"
          placeholder="placeholder"
          selectedValue={this.state.selectedCategory}
          onValueChange={value => this.setState({selectedCategory:value})}
          >
          <Item label="General" value="General" />
          <Item label="Work" value="Work" />
          <Item label="Project" value="Project" />
          <Item label="Home" value="Home" />
        </Picker>
      </Item>
      
    )
  }
  render() {
    return (
      <View style={styles.InputContainer}>
      <Item>
        <Icon name="ios-create" style={{fontSize:30,color:"#CCC"}}/>
        <Input 
          style={styles.InputBox} 
          placeholder='Add Todo...' 
          onChangeText={item => this.setState({item:item})}
          value={this.state.item} />
        { this.state.item == '' ? null: this._actions() }  
      </Item>  
        {this.state.showCategory == true && this.state.item != '' ? this._categoriesForm():null}
      </View>
    )
  }
}


const styles = StyleSheet.create({
  InputContainer:{
    marginBottom:20,
    backgroundColor:'#FFF',
    marginHorizontal:10,
    // marginTop:50
  },
  InputBox:{
    // padding:15,
    // backgroundColor:"#F1F1F1",
    // marginBottom:40
  },
  categoryList:{
    padding:5,
    
  }
});