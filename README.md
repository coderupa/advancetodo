This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

Sebelum menjalankan project ini. Instal terlebih dahulu [JSON Server](https://github.com/typicode/json-server). Kemudian start JSON server :

```$ json-server server/db.json --watch```
 