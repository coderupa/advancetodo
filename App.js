import React, { Component } from 'react'
import { StyleSheet,Platform} from 'react-native'
import TodoInput from './components/TodoInput'
import TodoItems from './components/TodoItems'
import TodoFooter from './components/TodoFooter'
import Sidebar from './components/Sidebar'
import API from './api'
 
import {
  StyleProvider,
  Container,
  Header,
  Body,
  Title,
  Content,
  Footer,
  Left,
  Right,
  Icon,
  Text,
  Button,
  Picker,
  Spinner,
  Item
} from 'native-base'

import theme from './theme/variables/theme'
import getTheme from './theme/components'
 
 
const unique_id = () => {
  return Math.random().toString(36).substr(2,4);
}

export default class App extends Component {
  state={
    isReady:false,
    selectedCategory:'All',
    todos:[],
    filteredTodos:[]
  }
 
  async componentWillMount(){
    await Expo.Font.loadAsync({
      'Roboto' : require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium' : require('native-base/Fonts/Roboto_medium.ttf'),
      'Ionicons' :require('native-base/Fonts/Ionicons.ttf')
    })
    this.setState({
      isReady:true
    })
  }

  async componentDidMount() {
    let item = await API.readTodos()
    await this.setState({
      todos:item
    })
    await this._actionFilter()
  }
  
  _actionAdd=async (item,category,done=false) => {
    let id=unique_id()
    let todos=this.state.todos
    let date=new Date()
    await API.createTodo(id,item,category,done,date)
    await this.setState({
      todos:[...todos,{id,item,category,done,date}]
    })
    await this._actionFilter()
  }

  _actionFinish= async (id)=> {
    let todos=this.state.todos
    let todoEdited
    let newTodos=todos.map(todo => {
      if(todo.id == id){
        todo.done = !todo.done
        todoEdited=todo
      }
      return todo
    })
    await API.updateTodo(todoEdited)
    await this.setState({
      todos:newTodos
    })
    await this._actionFilter()
  }

  _actionDelete =  async(id) => {
    let todos=this.state.todos
    let newTodos=todos.filter(todo => todo.id !== id)
    await API.deleteTodo(id)
    await this.setState({
      todos:newTodos
    })
    await this._actionFilter()
  }

  _actionFilter=(category='All')=>{
    let todos=this.state.todos
    let newTodos=todos
    if (category !== 'All'){
      newTodos=todos.filter(todo => todo.category == category)
    }
    newTodos.sort(function compare(a, b) {
      var dateA = new Date(a.date);
      var dateB = new Date(b.date);
      return dateB - dateA
    });
    this.setState({
      filteredTodos:newTodos,
      selectedCategory:category
    })
  }

  render() {
    if (this.state.isReady == false){
      return <Expo.AppLoading/>
    }
    return (
      <StyleProvider style={getTheme(theme)}>
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={ () => null} >
                <Icon name="logo-buffer"/>
              </Button>
            </Left>
            <Body>
              <Title>TodoApp</Title>
            </Body>
            <Right>
              <Picker
                style={styles['categoryPicker_'+ Platform.OS]}
                iosHeader="Select Category"
                headerBackButtonText= {<Icon name="ios-arrow-back" style={{color:"#FFF",fontSize:20,fontWeight:'bold'}}/>}
                mode="dialog"
                placeholder={<Icon style={{fontSize:20,color:"#FFF"}} name="ios-albums" />}
                textStyle={{color:"#FFF",fontSize:14}}
                selectedValue={this.state.selectedCategory}
                onValueChange={value => this._actionFilter(value)}
              >
                <Item label="All" value="All" />
                <Item label="General" value="General" />
                <Item label="Work" value="Work" />
                <Item label="Project" value="Project" />
                <Item label="Home" value="Home" />
              </Picker>
            </Right>
          </Header>
          <Content>
            <TodoInput actionAdd={this._actionAdd}/>
            {this.state.filteredTodos.length == 0 ? <Text style={{textAlign:'center'}}>No task Found. It's time to work.</Text>:null}
            <TodoItems 
              todos={this.state.filteredTodos} 
              actionFinish={this._actionFinish} 
              actionDelete={this._actionDelete} 
            />
          </Content>
        </Container>
      </StyleProvider>
 
    );
  }
}
 
const styles=StyleSheet.create({
  categoryPicker_ios:{
    flex: 1,
  },
  categoryPicker_android:{
    color: "#FFF",
    flex: 1,
  }
})