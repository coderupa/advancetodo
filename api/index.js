// const endpoint='http://192.168.1.100:3000/todos'
const endpoint='http://localhost:3000/todos'
class API{
  
  async readTodos(){
    try {
      let response = await fetch(endpoint, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json' 
        }
      });
      return await response.json()  
    } catch (error) {
      console.log('error found ' + error)
    }
  }

  async createTodo(id,item,category,done,date){
    let data={
      id,
      item,
      category,
      done,
      date
    }
    try {
      await fetch(endpoint, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json' 
        },
        body:JSON.stringify(data)
      })
    } catch (error) {
      console.log('error found ' + error)
    }
  }

  async deleteTodo(id){
    try {
      await fetch(endpoint+'/'+id, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json' 
        }
      })
    } catch (error) {
      console.log('error found ' + error);
    }
  }

  async updateTodo({id,item,done,date,category}){
    let data={
      id,
      item,
      category,
      done,
      date
    }
    try {
      await fetch(endpoint+'/'+id, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body:JSON.stringify(data)
      })
    } catch (error) {
      console.log('error found ' + error);    
    }
  }

}

export default new API()